#! /usr/bin/env python3
print('Content-type: text/html')
print()

import cgi, cgitb
import pythontest
import a3

cgitb.enable()
form = cgi.FieldStorage()
query = form.getfirst("query", "test_default").upper() 
query = a3.main(query)
print ("<html>")
print ("<head>")
print ("<title>Results</title>")
print ("</head>")
print ("<body>")
print ("<h2> %s </h2>" % (query))
print ("</body>")
print ("</html>")
