'''
Payton Thackery
i427
Assignment 3
'''

import os.path
from bs4 import BeautifulSoup  
import nltk.stem.porter as p
import sys
from cmath import log

'''
---------------Part 1----------------
'''

'''
---readIndexFileIntoDict---
function: converts index file into dict
Takes:
    (1) file that contains file names and urls associated with that file
Returns:
    (1) dictionary of form: {'fileX.html': sampleUrl, ...} 
'''

def readIndexFileIntoDict(indfile):
    #open and read file
    text_file = open(indfile, "r")
    #split apart file into list
    textArray = text_file.read().split()
    #close file
    text_file.close()
    
    textArray = cleanArray(textArray)
 
    #setup dict 
    newTextDict = {}    
    for x in range(0, len(textArray), 2):
        filename = textArray[x]
        url = textArray[x + 1]
        newTextDict[filename] = url

    return newTextDict

'''
---cleanArray---
function:
        (1) iterates through array to clean out excess characters 
Takes:
        (1)array: array of filenames and urls
            ex. ["{'fileX.html':", "'url',", ...]
Returns:
        cleanedArray of form:
            {'fileX.html', 'urlName', ....^repeat^.... }
                (1)fileX.html is the name of the file the urlText is saved in
                (2)urlName is the name of the url text originated from

'''    
def cleanArray(array):
    firstFile = True
    cleanedArray = []
    for file in range(0, len(array), 1):
        string = array[file]
        if not firstFile:
            string =  string[1:-2]
        else: 
            string =  string[2:-2]
            firstFile = False   
        cleanedArray.append(string)
    return cleanedArray     
  
'''
---readFileTermsIntoArray---
function: creates array of terms from files
Takes: directory of where files are located, dictionary of what the files are named, directory where stopwords are located
Returns: {'InvertedDictionary':invIndexDict, 'DocsDictionary':docsDict} 
'''               
def readFileTermsIntoArray(directory, fileDictionary, directorySWords):
    termsArray= []
    wordsFileDict = {}
    invIndexDict = {}
    docsDict = {}
    
    sWords = importSWords(directorySWords)
    for file in fileDictionary:
        filepath = directory + '/' + file
        singleFile = readSingleFileIntoArray(filepath, sWords)
        oneFileTerms = singleFile['arrayWords']
        numWords = singleFile['numWordsInFile']
        url = fileDictionary[file]
        
        #insert title here
        text_file = open(filepath, encoding="utf8")
        soup = BeautifulSoup(text_file)
        try:
            title = soup.title.string.encode(sys.stdout.encoding, errors='replace')
        except:
            title = "NULL TITLE"
        
        docsDict[file] = (numWords, url, title)
        #setup terms array
        for term in oneFileTerms:
            if term not in termsArray:
                termsArray.append(term)
                
        wordsFileDict[file] = oneFileTerms
    
    firstRun = True
    for file in wordsFileDict:
        #print(file)
        if firstRun:
            for term in termsArray:
                numFound = findFreqInFile(wordsFileDict[file], term)
                invIndexDict[term] = [(file, numFound)]
            firstRun = False
        else:
            for term in termsArray:
                numFound = findFreqInFile(wordsFileDict[file], term)
                if numFound != 0:
                    invIndexDict[term].append((file, numFound))
    #save invDict to new file: invIndex.dat
    file = 'invIndex.dat'
    file = os.path.join(directory, file)
    file = open(file,"w")
    file.write(str(invIndexDict))
    file.close()
    #save docsDict to new file: docs.dat
    file = 'docs.dat'
    file = os.path.join(directory, file)
    file = open(file,"w")
    file.write(str(docsDict))
    file.close()    
    
    return {'InvertedDictionary':invIndexDict, 'DocsDictionary':docsDict}   
                
'''
---findFreqInFile---
function: finds how often a given term is in the file
Takes: array of words, word to search for within that array
Returns: number of times the word appears in the array
''' 
def findFreqInFile(oneFileTermsArray, term):
    numFound = 0
    for word in oneFileTermsArray:
        if word == term:
            numFound += 1
    return numFound

'''
---importSWords---
function: creates an array of stopwords given text file of words
Takes: directory where stopwords.txt is located
Returns:
'''
def importSWords(directory):
    fileLoc = directory + '/' + 'stopwords.txt'
    #open and read file
    text_file = open(fileLoc, "r")
    #split apart file into list
    swordsArray= text_file.read().split('\n')
    #close file
    text_file.close()
    
    return swordsArray
'''
---readSingleFileIntoArray---
function: goes through one file and places cleaned words into array
Takes: filename, stopwords to be removed from terms
Returns: array of cleaned terms
'''    
def readSingleFileIntoArray(filename, sWords):
    
    #read file into array
    text_file = open(filename, encoding="utf8")
    soup = BeautifulSoup(text_file)
    text = soup.get_text()
    textArray = text.split()
    numWords = len(textArray)
    text_file.close()
    
    #remove stopwords
    stopArray = []
    for word in textArray:
        if word not in sWords and not(len(word) > 30):
            stopArray.append(word)

    #stem list
    stemmedList = []
    stemmer = p.PorterStemmer()
    for word in stopArray:
        stemmedList.append(stemmer.stem(word))
        
    #remove weird characters
    cleanedArray = []
    goAwayList = ["''", ":", ";", ",","-", ".", "=", "(", "/", ")", "^|\\s", "$", "/", "``", "{", "}", "]", "["]
    for word in stemmedList:
        if word not in goAwayList:
            cleanedArray.append(word)
    cleanerArray = []        
    for word in cleanedArray:
        goodWord = True
        for letter in word:
            if letter in goAwayList:
                goodWord = False
                break
        if goodWord:
            cleanerArray.append(word)
    
    #ensure non-standard characters are converted into readable format
    for i in range(0, len(cleanerArray), 1):
        cleanerArray[i] = cleanerArray[i].encode(sys.stdout.encoding, errors='replace')   
    return{'arrayWords':cleanerArray, 'numWordsInFile':numWords}   
            
'''
---indexer---
function: finds all good terms in files
Takes: directory of files, index file, directory where stopwords file is located
Returns: {'InvertedDictionary':invIndexDict, 'DocsDictionary':docsDict} 
'''    
def indexer(directory, indexFile, directorySWords):
    fileDict = readIndexFileIntoDict(indexFile)
    return readFileTermsIntoArray(directory, fileDict, directorySWords)
    
'''
---------------Part 2-------------
''' 

'''
---retrieval---
function: finds appropriate files that fit the given mode and terms
Takes: 
    (1) mode of form: 'and' 'or' 'most'
    (2) list of words
    (3) inverted dictionary of form:
            {term: [(fileName, freq), ...], ...}
Returns:
    array of files fitting mode
''' 
def retrieval(mode, listWords, invDict, docsDict):
    #stem list
    stemmedList = []
    stemmer = p.PorterStemmer()
    for word in listWords:
        stemmedList.append(stemmer.stem(word))
    
    #ensure non-standard characters are converted into readable format    
    for i in range(0, len(stemmedList), 1):
        stemmedList[i] = stemmedList[i].encode(sys.stdout.encoding, errors='replace')   
     
    files = []
    numSearched = 0
    
    if mode == 'and':
        andFunReturn = andFunc(stemmedList, invDict)
        files = andFunReturn[0]
        numSearched = andFunReturn[1]
    elif mode == 'or':
        orFunReturn = orFunc(stemmedList, invDict)
        files = orFunReturn[0]
        numSearched = orFunReturn[1]
    elif mode == 'most':
        mostFunReturn = mostFunc(stemmedList, invDict)
        files = mostFunReturn[0]
        numSearched = mostFunReturn[1]
    else:
        print('not a valid query mode')
        return
    
    titleDict = findTitle(files, docsDict)
    return(titleDict, numSearched)

'''
----findTitle----
function: takes array of files and finds their title
Takes: list of files
Returns: dict of files with their titles {(file, title), ...}
'''
def findTitle(listFiles, docsDict):
    fileTitleDict = {}
    
    for file in listFiles:
        info = docsDict[file]
        title = info[2]       
        fileTitleDict[file] = title
        
    return fileTitleDict


'''
---andFunc---
function: return pages that include all of the keywords
Takes: list of words to look for, inverted dictionary
Returns: array of files that include all of the keywords
''' 
def andFunc(listWords, invDict):
    fileArray = []
    
    numSearch = 0
    #ensure query exists in key terms
    for word in listWords:
        if word not in invDict:
            print('query word- ', word, '- is not in files')
            return
        
    #setup array that contains list of files with with first word
    arrayFiles = invDict[listWords[0]]
    for x in range(0, len(arrayFiles), 1):
        fileArray.append(arrayFiles[x][0])
    
    #eliminate files that do not have the rest of the words 
    for word in range(1, len(listWords), 1):
        tuples = invDict[listWords[word]]
        files = []
        for x in range(0, len(tuples), 1):
            files.append(tuples[x][0])
            numSearch += 1
        for file in fileArray:
            if file not in files:
                fileArray.remove(file)
            numSearch += 1
    tuple = (fileArray, numSearch)            
    #print(fileArray)
    return tuple

'''
---orFunc---
function: find pages that include any of the keywords
Takes: list of words to look for, inverted dictionary
Returns: array of files that include any of the keywords
'''
def orFunc(listWords, invDict):
    fileArray = []
    numSearched = 0
    
    #eliminate words not in the dictionary
    vettedWords = listWords
    for word in listWords:
        if word not in invDict:
            vettedWords.remove(word)
            print('query word- ', word, '- is not in files')
    listWords = vettedWords
    
    
    #add files with 1 or more of the given words
    for word in range(0, len(listWords), 1):
        tuples = invDict[listWords[word]]
        files = []
        
        for x in range(0, len(tuples), 1):
            files.append(tuples[x][0])
            numSearched += 1
        for file in files:
            if file not in fileArray:
                fileArray.append(file)
            numSearched += 1
            
    #print(fileArray)
    return(fileArray, numSearched)

'''
---mostFunc---
function: find pages that include most (at least half) of the keywords
Takes: list of words to look for, inverted dictionary
Returns: array of files that include most of the keywords
'''
def mostFunc(listWords, invDict):
    fileTermDict = {}
    numSearched = 0
    
    #eliminate words not in the dictionary
    vettedWords = listWords
    for word in listWords:
        if word not in invDict:
            vettedWords.remove(word)
            print('query word- ', word, '- is not in files')
    listWords = vettedWords
    
    #add files to dictionary with list of words each file has from given words
    for word in range(0, len(listWords), 1):
        tuples = invDict[listWords[word]]
        files = []
        
        for x in range(0, len(tuples), 1):
            files.append(tuples[x][0])
            numSearched += 1
            
        for file in files:
            if file not in fileTermDict:
                fileTermDict[file] = [listWords[word]]
            else: 
                fileTermDict[file].append(listWords[word])
            numSearched += 1
                
    #check which files have greater than or equal to half the words
    fileArray = []
    halfNumWords = (len(listWords) / 2)
    for file in fileTermDict:
        filelistLen = len(fileTermDict[file])
        if(filelistLen >= halfNumWords):
            fileArray.append(file)
        numSearched += 1
    
    #print(fileArray)
    return(fileArray, numSearched)

'''
Returns: {file: (numOccurencesInDoc, numTermsInDoc, numDocs, numDocsWithTerm)}  
        where file = fileX.html
''' 
def tfIDFB(listWords, invDict, docsDict, filesDict):
    #stem list
    stemmedList = []
    stemmer = p.PorterStemmer()
    for word in listWords:
        stemmedList.append(stemmer.stem(word))
    
    #ensure non-standard characters are converted into readable format    
    for i in range(0, len(stemmedList), 1):
        stemmedList[i] = stemmedList[i].encode(sys.stdout.encoding, errors='replace') 
    
    #begin tfidf calculation    
    numDocs = len(docsDict)
    returningDict = {}
    files = []
    
    for key in filesDict:
        files.append(key)
    
    for file in files:
        numOccurences = 0
        numDocsWithTerm = 0
        for word in stemmedList:
            for tuples in invDict[word]:
                if tuples[0] == file:
                    numOccurences += tuples[1]
                    break
            numDocsWithTerm += len(invDict[word])
        
        numTerms = docsDict[file][0]
        url = docsDict[file][1]
        title = docsDict[file][2]
        tf = numOccurences / numTerms
        idf = log(numDocs / numDocsWithTerm)
        score = tf * idf
        returningDict[url] = (title, score)

        
    print(returningDict)
    return returningDict
    

def main(listWords):
    '''
    testing: (non-user input)
    '''   
    sampleUrl = "https://en.wikipedia.org/wiki/Special:Random"
    sampleDir = '/nfs/nfs7/home/pcthacke/courses/i427/projects/htmlfilesB'
    index = '/nfs/nfs7/home/pcthacke/courses/i427/projects/htmlfilesB/index.dat'
    directorySWords = '/nfs/nfs7/home/pcthacke/courses/i427/projects'
    dicts = indexer(sampleDir, index, directorySWords)
    invdict = dicts['InvertedDictionary']
    docsdict = dicts['DocsDictionary']
    
    listWords = ['like','help', 'jump', 'encyclopedia', 'wild', 'najarula', 'hosenera']
    #retrieval('and', ['like','help'], invdict, docsdict)
    retrieved = retrieval('or', listWords, invdict, docsdict)
    #retrieved = retrieval('most', ['like','help', 'jump', 'encyclopedia', 'wild', 'najarula', 'hosenera'], invdict, docsdict)
    retrievedTitleDict = retrieved[0]
    #retrievedNumGoThrough = retrieved[1]
    tf = tfIDFB(listWords, invdict, docsdict, retrievedTitleDict)
    
    return tf
    
    
main()


